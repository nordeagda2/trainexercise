import java.util.Observable;
import java.util.Observer;

/**
 * Reprezentuje poruszający się pociąg.
 * Created by amen on 9/13/17.
 */
public class Train extends Observable implements Runnable {

    private int stationId;
    private boolean direction; //true is increment, false is decrement;
    private TrainSchedule schedule;

    public Train(int stationId, boolean direction, TrainSchedule schedule) {
        this.stationId = stationId;
        this.direction = direction;
        this.schedule = schedule;
    }

    public void run() {
        // każdy pociąg porusza się po linii
        // każdy pociąg jeśli zatrzymuje sie na stacji, to czeka na niej dokładnie 1 sek.
        // każdy pociąg może być anulowany
        // każdy pociąg informuje headquarters o tym że dojechał na stację, nawet jeśli nie zatrzymuje się na niej (nie czeka 1s.)
    }
}
